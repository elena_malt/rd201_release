var ClientType = project.getClientType();

///********************************************************************************************************************************************

function errorLog(text){
	if( project ){
		if( project.getClientType() == "web" ) console.log(text);
		else if( project.getClientType() == "local" ) alert(text); //print(text);
	}
	return 1;
}

/// - - - - - - - - -
/// - - - - - - - - -
/// JAVASCRIPT NATIVO
/// - - - - - - - - -
/// - - - - - - - - -



/// - - - - - - - - -
/// - - - - - - - - -
/// JAVASCRIPT PER WEB
/// - - - - - - - - -
/// - - - - - - - - -

/// Language

// MFD a quanto pare pulisce la cache al riavvio, e non ricorda la lingua.
// All'avvio, il client legge il tag della lingua dal PLC e imposta la propria lingua (non del server).
// Usiamo la variazione del tag anche per modificare la lingua dall'interfaccia.
function _projectLanguageMgr_onDataUpdate(me, eventInfo)
{
	try{
		if( project.getClientType() == "web" ){
			if( eventInfo.attrName == "value1" ){
				var language_id = eventInfo.newValue;
				var MultiLangMgr = project.getWidget( "_MultiLangMgr" );
				if( MultiLangMgr ){
					MultiLangMgr.setProperty("curLangId", language_id);
//alert("Setting language to " + language_id + " (client type: " + project.getClientType() + ")");
				}
			}
		}//getClientType
	} catch(e) {
		return errorLog("Multilanguage_JSFunctBlock_onDataUpdate - Error: " + e);
	}
	return false;
}

project.themes = ["day", "hc"];

function toggleTheme(pageId){
	try{
		if( project.getClientType() == "web" ){
			var srcTheme = pageId.match(/(_day|_hc)/g);

			var expressionString = "(_" + project.themes.join('|_') + ")"; 	// Espressione: /(_day|_hc)/g --> "(_day|_hc)" 	// Poi per quanto a `g` vediamo se è il caso...
			var expression = new RegExp(expressionString, "g");
			var i = 0;
			while( (("_" + project.themes[i]) != pageId.match(expression)) && (i < project.themes.length) ){
//console.log("toggleTheme - project theme: " + "_" + project.themes[i] + "; \tMatch: " + pageId.match(expression));
				i++;
			} // A questo punto i == indice del tema della pagina pageId, oppure i == project.themes.length.
			if( i >= project.themes.length ) errorLog("toggleTheme - Assertion fail: invalid theme for page " + pageId);
			else {
				project.activeThemeIndex = i + 1;
				var newPageId = pageId.replace(expression, "_" + project.themes[project.activeThemeIndex]);
				console.log("toggleTheme - loading page: " + newPageId + "; \tsetting activeTheme to " + (i + 1));
				project.loadPage(newPageId);
			}
		}//getClientType
	} catch(e){
		errorLog("toggleTheme - Error: " + e);
	}
}
project.toggleTheme = toggleTheme;

if(ClientType=="web"){

	console.log("Itizialization");

	var doingHash = false;  // questo è un semaforo: se clicco sul popup della pagina allarmi //?

	// Get PageMgr widget
	var pageMgr = project.getWidget( "_PageMgr" );
	console.log("START")
	for (var a in pageMgr){
		console.log(pageMgr[a]);
	}
	console.log("END");
	console.log("INIT - current url: " + window.location.href);

	project.enableResize=1;



	//-------------------------------------------------------------------
	//----------------------WORKAROUNDS----------------------------------
	//-------------------------------------------------------------------

	/// Add scrollbar to alarms tables
	// Page with table should call `project.workarounds.enableWebKitScrollBar(page, "tableName")`, where tableName is the widget id.
	try{
		project.workarounds = {
			addWorkaround: function( name, wr )
			{
				if ( name !== "addWorkaround" )
					project.workarounds[name] = wr;
			}
		};
		project.workarounds.addWorkaround(
			"enableWebKitScrollBar",
			function( page, tableID )
			{
				if  ( project.getClientType() == "web" )
				{
					var wgt = page.getWidget( tableID );
					if ( wgt )
					{
						var unsafewgt = wgt.wgt;
						if ( unsafewgt.elem )
						{
							var thead = unsafewgt.elem.childNodes[1];
//							if ( thead )
							if( thead && thead.className )
							{
								if ( thead.className.indexOf( "webkitbar" ) == -1 )
								{
								thead.className = thead.className + " webkitbar";
								}
							}
							if ( unsafewgt.elem.children[0] )
							{
								var contentpane = unsafewgt.elem.children[0].children[1]
								if ( contentpane )
								{
									if ( contentpane.className.indexOf( "webkitbar" ) == -1 ){
										contentpane.className = contentpane.className + " webkitbar";
									}
								}
							}
						}
					} else {
						alert("Table workaround - Warning, no widget!");
					}
				}
			}
		);

		project.workarounds.addWorkaround(
			"forceNativeSelector",
			function()
			{
	//			hmiWidget.prototype._isNativeSelectCompatible = function(){
				$hmi.prototype._isNativeSelectCompatible = function(){
					return false;
				};
			}
		);

		// Remove comment from next line to try native selector
		project.workarounds.forceNativeSelector();
	} catch(e){
		alert("Table workaround - Error: " + e);
	}


	/// To avoid the OnPress freeze when lose focus
	function simulateClickOnLoseFocus()
	{
		function generateUpEvent()
		{
console.log("generateUpEvent");
			$hmi.mouseMgr.loseFocus();
		}
		window.addEventListener('blur', generateUpEvent, false);
		window.addEventListener('focus', generateUpEvent, false);
	}

	simulateClickOnLoseFocus();


	document.addEventListener('keydown', function(event){
		if (event.code === "ArrowLeft")
		  project.prevPage();
		else if (event.code === "ArrowRight")
		  project.nextPage();
	} );


	//-------------------------------------------------------------------
	//----------------------CAMBIO PAGINA FRAMENT------------------------
	//-------------------------------------------------------------------
	//TODO:
	//Flavio -> non cambiare window.location ogni volta che cambio pagina. Si può fare override?
	//Flavio -> utilizzare ash anche al posto di Alias ed evitare loading.html?loadpage=pagina1
	//Navico -> togliere parametri da url
	//Navico -> implementare CAL con hash anziché con





	window.addEventListener("hashchange", HashChanged);
	//console.log('project:' + window.location.href);

	function cleanHref(){
console.log("cleanHref - URL: " + window.location.pathname + window.location.search);
		history.pushState("", document.title, window.location.pathname + window.location.search);
	}

	function HashChanged(){
		try{
console.log("> HashChanged");
console.log("  location.href: " + window.location.href);
console.log("  location.hash: " + window.location.hash);
			var pageMgr = project.getWidget("_PageMgr");
			if( pageMgr ){
				if( window.location.hash == '#' + pageMgr.getCurrentPageName() ){
					cleanHref();
				}
			}
			var newPage= location.hash.replace('#','');
			project.loadPage(newPage+'.jmx');
		} catch(e){
			alert("HashChanged - Error: " + e);
		}
	}





	//-------------------------------------------------------------------
	//----------------------LETTURA PARAMETRII URL-----------------------
	//-------------------------------------------------------------------
	function getURLParameter(sParam){
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++){
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam){
				return sParameterName[1];
			}
		}
		return "false";
	}





	//-------------------------------------------------------------------
	//---------------------------- PRECACHE -----------------------------
	//-------------------------------------------------------------------

	//CACHING - Available from ND 2.8
	//project.setProperty( "pagesCacheSize", 10 ) ;
	//lo faccio già quando ho finito il pracachin //project.changePageAnimation(true); // adding animates spinner on page changing
	project.setProperty( "changePageAnimationOnCache", false ); // disable spinner if new page has been already cached

	$hmi.limitPages = 10;

	project.homeFull		= "engine_nav-engine1_day_full";
	project.homeHalf		= "engine_nav-engine1_day_half";
	//project.homeRow		= "engine_nav-engine1_day_row"; //OPT
	//project.homeCol		= "engine_nav-engine1_day_col"; //OPT
	project.comErrorPageName = "engine_comerror_day";
	project.comErrorFull	= "engine_comerror_day_full";

	//scelta pagine da cacheare
	var width = window.innerWidth;
	var height = window.innerHeight;
	var viewportRatio=width/height;

	// Le scelgo direttamente senza inserire il check pagine half / full /colonna



	/*if(viewportRatio<0.5 && project.Video==1){   //Column
		console.log("Precache - array column");
		var precachedPages=["control1_col"];

		//in questo caso il precache finisce subito. C'è comqunque un brutto effetto
		//dovuto all'immagine Naviop al centro del waiting div che viene tagliata. Whatever
	}
	else*/ if(viewportRatio<1 ){	//Half
		console.log("Precache - array half");
		var precachedPages=["engine_comerror_day_half",
							"engine_nav-engine1_day_half", "engine_alarms_day_half",
							"engine_nav-engine1_hc_half", "engine_alarms_hc_half",
							"engine_comerror_day_full", "engine_comerror_day_half" //le metto sempre in cache
				];
	}
	else if(viewportRatio<3 ){	//Full
		console.log("Precache - array full");
		var precachedPages=["engine_comerror_day_full",
							"engine_nav-engine1_day_full", "engine_nav-engine2_day_full", "engine_nav-service_day_full", //"engine_alarms_day_full", "engine_set-usersetup_day_full", "engine_historyalarms_day_full",
							"engine_nav-engine1_hc_full", "engine_nav-engine2_hc_full", "engine_nav-service_hc_full", //"engine_alarms_hc_full", "engine_set-usersetup_hc_full", "engine_historyalarms_hc_full",
							"engine_nav-engine1_day_full",
							"engine_comerror_day_full", "engine_comerror_day_half" //le metto sempre in cache
				];
	}

	var percacheIndex=0;
	var waitingDiv;
	var progressBar;
	var progressBarFrame;
	var precachedComplete = false;

	project.precachePages=function fun(){
		console.log("Precache - inizio precache");
		percacheIndex=0;
		precachedComplete = false;

		//Copro con un DIV per coprire i cambi pagina
		waitingDiv = document.createElement('div');
		waitingDiv.style.display = "block";
		waitingDiv.style.visibility = "visible";
		waitingDiv.style.position = "absolute";
		waitingDiv.style.left = "0";
		waitingDiv.style.top = "0";
		waitingDiv.style.height = "100%";
		waitingDiv.style.width = "100%";
		waitingDiv.style.background="black";
		waitingDiv.style.overflow="hidden";
		waitingDiv.style.textAlign="center";
		waitingDiv.style.verticalAlign="middle";
		waitingDiv.scrolling="no"

		var txtDiv = document.createElement('div');
		txtDiv.style.display = "block";
		txtDiv.style.visibility = "visible";
		txtDiv.style.position = "absolute";
		txtDiv.style.left = "0%";
		txtDiv.style.top = "90%";
		txtDiv.style.width = "100%";
		txtDiv.style.textAlign="center";
		var par = document.createElement("P");
		var txt1 = document.createTextNode('Loading');
		par.appendChild(txt1);
		par.style.fontSize = "xx-large";
		par.style.fontFamily = "sans-serif";
		par.style.color = "white";
		txtDiv.appendChild(par);
		waitingDiv.appendChild(txtDiv);
/*
		var img=document.createElement("img");
		img.style.position="absolute";
		img.src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB2ZXJzaW9uPSIxLjEiCiAgIHdpZHRoPSIzMyIKICAgaGVpZ2h0PSIzMyIKICAgdmlld0JveD0iMCAwIDMzIDMzIgogICBpZD0iTGl2ZWxsb18xIgogICB4bWw6c3BhY2U9InByZXNlcnZlIj48bWV0YWRhdGEKICAgaWQ9Im1ldGFkYXRhMzIiPjxyZGY6UkRGPjxjYzpXb3JrCiAgICAgICByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUKICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48ZGM6dGl0bGU+PC9kYzp0aXRsZT48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGRlZnMKICAgaWQ9ImRlZnMzMCI+CgkKCQo8L2RlZnM+Cgo8ZwogICB0cmFuc2Zvcm09Im1hdHJpeCgwLjg0ODQ4NDgsMCwwLDAuODQ4NDg0OCwyMC4yMDIyNjIsNC42NjM1MzM5KSIKICAgaWQ9ImczMTMzIj48ZwogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuMDc3NTEwMjgsMCwwLDAuMDc3NTEwMjgsLTIwLjg2MzM4MSwtMi41NDk4NzkzKSIKICAgICBpZD0iZzUiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZiI+PHBhdGgKICAgICAgIGQ9Ik0gMjEyLjg3NSw0MjUuNzUgQyA5NS40OTQsNDI1Ljc1IDAsMzMwLjI1NiAwLDIxMi44NzUgMCw5NS40OTQgOTUuNDk0LDAgMjEyLjg3NSwwIDMzMC4yNTYsMCA0MjUuNzUsOTUuNDk0IDQyNS43NSwyMTIuODc1IGMgMCwxMTcuMzgxIC05NS40OTQsMjEyLjg3NSAtMjEyLjg3NSwyMTIuODc1IHogbSAwLC0zNTQuMDU5IGMgLTc3Ljg0OCwwIC0xNDEuMTg1LDYzLjMzNSAtMTQxLjE4NSwxNDEuMTg0IDAsNzcuODQ5IDYzLjMzNiwxNDEuMTg1IDE0MS4xODUsMTQxLjE4NSA3Ny44NDgsMCAxNDEuMTg0LC02My4zMzYgMTQxLjE4NCwtMTQxLjE4NSAwLC03Ny44NDkgLTYzLjMzNiwtMTQxLjE4NCAtMTQxLjE4NCwtMTQxLjE4NCB6IgogICAgICAgaWQ9InBhdGg3IgogICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZiIgLz48L2c+PHBhdGgKICAgICBkPSJtIC00LjY4NDExODUsOC4zMTI0OTA5IGMgLTEuNTM0MzkzLDAgLTIuNzc4NDMzLDEuMjQzODggLTIuNzc4NDMzLDIuNzc4NDMwMSAwLDAuNDQxOTYgMCw1LjI0MTQgMCw1LjcxODQgMCwxLjUzNDQ3IDEuMjQ0MDQsMi43NzgzNSAyLjc3ODQzMywyLjc3ODM1IDEuNTM0Mzk0LDAgMi43NzgzNTYsLTEuMjQzOCAyLjc3ODM1NiwtMi43NzgzNSAwLC0wLjQwOTE4IDAsLTUuMjE2MjEgMCwtNS43MTg0IDAsLTEuNTM0NTUwMSAtMS4yNDM5NjIsLTIuNzc4NDMwMSAtMi43NzgzNTYsLTIuNzc4NDMwMSB6IgogICAgIGlkPSJwYXRoOSIKICAgICBzdHlsZT0iZmlsbDojZmZmZmZmIiAvPjwvZz48L3N2Zz4="
		img.style.height = "50%";
		//img.style.width = "50%";
		img.style.top = "0";
		img.style.top = "0";
		img.style.left = "0";
		img.style.right = "0";
		img.style.bottom = "0";
		img.style.margin = "auto";
		img.style.overflow = "auto";

		waitingDiv.appendChild(img);
*/
/*
		var txtDiv1 = document.createElement('div');
		txtDiv1.style.display = "block";
		txtDiv1.style.visibility = "visible";
		txtDiv1.style.position = "absolute";
		txtDiv1.style.left = "0%";
		txtDiv1.style.top = "50%";
		txtDiv1.style.width = "50%";
		txtDiv1.style.textAlign="center";
		var par1 = document.createElement("P");
		var txt2 = document.createTextNode('WARNING THIS ONBOARD MONITORING AND CONTROL SYSTEM IS DESIGNED TO FUNCTION AS AN AUXILLARY SYSTEM.THIS DATA IS DESIGNED TO AID TRADITIONAL NAVIGATION METHODS, NOT AS A REPLACEMENT FOR SOUND NAVIGATIONAL JUDGMENT.DO NOT RELY ON THIS PRODUCT AS A PRIMARY SOURCE OF NAVIGATION. THE OPERATOR IS RESPONSIBLE FOR USING OFFICIAL GOVERNMENT CHARTS AND PRUDENT METHODS FOR SAFE NAVIGATION WHEN USING THIS PRODUCT. THIS DATA MAY CHANGE WITHOUT PRIOR NOTICE AND MAY NOT BE THE LATEST UPDATE. THIS DISCLAIMER APPLIES BOTH TO INDIVIDUAL USE OF THE DATA AND AGGREGATE USE WITH OTHER DATA.');
		par1.appendChild(txt2);
		par1.style.fontSize = "xx-large";
		par1.style.fontFamily = "sans-serif";
		par1.style.color = "white";
		txtDiv1.appendChild(par1);
		waitingDiv.appendChild(txtDiv1);
*/

		progressBarFrame = document.createElement('div');
		progressBarFrame.style.display = "block";
		progressBarFrame.style.visibility = "visible";
		progressBarFrame.style.position = "absolute";
		progressBarFrame.style.left = "40%";
//		progressBarFrame.style.top = "85%";
		progressBarFrame.style.top = "45%";
		progressBarFrame.style.height = "20px";
		progressBarFrame.style.width = "20%";
		progressBarFrame.style.background="#919191";
		progressBarFrame.style.overflow="hidden";
		progressBarFrame.style.textAlign="center";
		progressBarFrame.style.verticalAlign="middle";
		progressBarFrame.style.borderRadius="16px";
		progressBarFrame.scrolling="no"

		progressBar = document.createElement('div');
		progressBar.style.display = "block";
		progressBar.style.visibility = "visible";
		progressBar.style.position = "absolute";
		progressBar.style.left = "0%";
		progressBar.style.top = "0%";
		progressBar.style.height = "100%";
		progressBar.style.width = "0%";
		progressBar.style.background="white";
		progressBar.style.overflow="hidden";
		progressBar.style.textAlign="center";
		progressBar.style.verticalAlign="middle";
		progressBar.style.borderRadius="16px";
		progressBar.scrolling="no"

		progressBarFrame.appendChild(progressBar);

		waitingDiv.appendChild(progressBarFrame);
		console.log("Precache - Mostro il DIV");
		document.body.appendChild(waitingDiv);

		//Faccio partire il cambio pagina ricorsivo

		// No more valid
		//var container = document.getElementById( "container" );
		//$hmi.getEventMgr().on( container, "hmiafterchange", precacheCallback);
	  // precacheCallback();

		// ------------ Update to workaround --------------
		// Align naviop workaround with jm4web precache flow
		project.on( "precacheend", function() {
			var container = document.getElementById( "jm-container" ); // Update to workaround
			$hmi.getEventMgr().on( container, "hmiafterchange",precacheCallback);
			precacheCallback();
		} );
		wrCheckAndTriggerWithSlowNetwork();
		// -----------------------------------------------

	}

	// updated
	function wrCheckAndTriggerWithSlowNetwork()
	{
		var pageMgr = project.getWidget( "_PageMgr" );
		if ( !pageMgr.wgt.isPrecaching() && pageMgr.wgt.waitPrecacheEnd() != null )
		{
			project.wgt.triggerEvent( "precacheend" );
		}
	}

	function precacheCallback(){
		var targetCachePage = precachedPages[percacheIndex];
		var currPage = pageMgr.getCurrentPageName();
		console.log("Precache - Current page: "+pageMgr.getCurrentPageName());
		console.log("Precache - Caching page: "+targetCachePage);
		console.log("Precache - loading: "+Math.floor(percacheIndex/precachedPages.length*100)+"%");
		console.log("Precache - loading: "+percacheIndex+"/"+precachedPages.length);
		setLoadingPercentage(percacheIndex/precachedPages.length*100);

		if ( targetCachePage != undefined ) {
			var dontSkip= ((targetCachePage != currPage)) ;
			var sizeCurr= currPage.substring(currPage.length-5);
			var sizeTrgt= targetCachePage.substring(targetCachePage.length-5);
			dontSkip= dontSkip && (sizeCurr == sizeTrgt || currPage == project.objectName);
			console.log("Precache - Current page size: "+sizeCurr);
			console.log("Precache - Caching page size: "+sizeTrgt);

			if( dontSkip ){
			console.log("Precache - cambio pagina: "+targetCachePage);
			project.loadPage(targetCachePage);
			percacheIndex++;
			}else{
			console.log("Precache - skipped page: "+targetCachePage);
			percacheIndex++;
			precacheCallback();
			}
		} else {
			if ( percacheIndex <= precachedPages.length ) {
				//hideLoadingbar();
				project.loadPage(project.homeFull);
				//project.changePageAnimation(true); // adding animates spinner on page changing
				project.setProperty( "changePageAnimationOnCache", false ); // disable spinner if new page has been already cached
				setTimeout(function(){
					if ( document.body.contains( waitingDiv ) ){
						document.body.removeChild(waitingDiv);
						// ------------ Update to workaround --------------
						var container = document.getElementById( "jm-container" );
						$hmi.getEventMgr().off( container, "hmiafterchange",precacheCallback);
						// -------------------------------------------------
					}
				}, 500 );
				percacheIndex++;
			}
		}
	}

	function hideLoadingbar(){
		if ( waitingDiv.contains( progressBarFrame ) ){
			waitingDiv.removeChild(progressBarFrame);
		}
	}

	function setLoadingPercentage(perc){
		progressBar.style.width = ""+Math.floor(perc)+"%";
	}



	project.precachePages();

/* Da LoopCore, verif
   // faccio il precache solo se non ho fatto lo SHOW degli allarmi (vecchio CAL)
   if(!getURLParameter("loadPage").includes("alarms")){
	  project.precachePages();
   }
*/
   /*
   // richiamo il pre-cache all'avvio
   project.on( "precacheend", function() { project.precachePages() } );
   */




	/************** GESTIONE CAMBI PAGINA E RESIZE  ****************/
//	// onUpdate di un tag CurrentTheme.
//	// 0 = day; 1 = hc;
//	function _pageChangeMgr_onDataUpdate(me, eventInfo){
//		try{
//			if( eventInfo.attrName == "value1" ){ 				// CurrentTheme
//				var newThemeIndex = eventInfo.newValue;
//				var prevTheme = project.currentTheme;
//				switch( newThemeIndex ){
//					case 1: 	project.currentTheme = "hc";
//					default:	project.currenttheme = "day";
//				}
//				console.log("_pageChangeMgr - Change to CurrentTheme: " + prevTheme + " → " + project.currentTheme);
//			}
//		} catch(e){
//			alert("_pageChangeMgr_onDataUpdate - Error: " + e);
//		}
//	}

	function new_resize(eventInfo){
console.log("> new_resize; doingHash: " + doingHash);

		console.log(doingHash);

		if (doingHash){
			console.log("Loading Hash page - preventing resize event")
			doingHash = false;
			return;
		}

		var width = eventInfo.width;
		var height = eventInfo.height;
		var viewportRatio=width/height;
		var currentPage=pageMgr.getCurrentPageName();

		var FullIdx=currentPage.indexOf("_full"); // se non trova la sottostringa, restituisce -1
		var HalfIdx=currentPage.indexOf("_half"); // se non trova la sottostringa, restituisce -1
		var ColIdx=currentPage.indexOf("_col"); // se non trova la sottostringa, restituisce -1
		var RowIdx=currentPage.indexOf("_row"); // se non trova la sottostringa, restituisce -1
		var pageName = "home";
		if(FullIdx>0){
		 pageName = currentPage.slice(0,FullIdx) ;
		}
		if(HalfIdx>0){
		 pageName = currentPage.slice(0,HalfIdx) ;
		}
		if(ColIdx>0){
		 pageName = currentPage.slice(0,ColIdx) ;
		}
		if(RowIdx>0){
		 pageName = currentPage.slice(0,RowIdx) ;
		}

		var targetPage = newTargetPage(viewportRatio, pageName);

		if (currentPage==targetPage){
			console.log("resizing");
			return null;					//se la pagina di arrivo è quella giusta faccio il resize.
		}else{
			console.log("changing page");
			project.loadPage(targetPage);
			return false;				 //altrimenti cambio pagina e blocco il resize.
		}
	}

	function new_pageChange(eventInfo){
console.log("> new_pageChange: " + eventInfo.src + " → " + eventInfo.dst);
console.log("  location.href: " + window.location.href);
console.log("  location.hash: " + window.location.hash);
		cleanHref();
		var currentPage = eventInfo.src;
		var destinationPage = eventInfo.dst;
		var width = eventInfo.width;
		var height = eventInfo.height;
		var viewportRatio=width/height;

//
//if( window.location.hash.replace('#','') == eventInfo.dst ){
//	window.location.hash = "";
//}
//
		if (destinationPage!=null)
		{
			var FullIdx=destinationPage.indexOf("_full"); // se non trova la sottostringa, restituisce -1
			var HalfIdx=destinationPage.indexOf("_half"); // se non trova la sottostringa, restituisce -1
			var ColIdx=destinationPage.indexOf("_col"); // se non trova la sottostringa, restituisce -1
			var RowIdx=destinationPage.indexOf("_row"); // se non trova la sottostringa, restituisce -1
		}else{
			var FullIdx= -1;
			var HalfIdx= -1;
			var ColIdx= -1;
			var RowIdx= -1;
		}
		var pageName = "home";
		if(FullIdx>0){
			pageName = destinationPage.slice(0,FullIdx) ;
		}
		if(HalfIdx>0){
			pageName = destinationPage.slice(0,HalfIdx) ;
		}
		if(ColIdx>0){
			pageName = destinationPage.slice(0,ColIdx) ;
		}
		if(RowIdx>0){
			pageName = destinationPage.slice(0,RowIdx) ;
		}

		console.log("  new_pageChange - viewportRatio: "+viewportRatio+" pagina: "+pageName);
		var targetPage = newTargetPage(viewportRatio, pageName);
		console.log("  new_pageChange - Nuova pagina: "+targetPage);
		console.log("  new_pageChange - Destination page: "+destinationPage);

		if (destinationPage.indexOf("_day") > 0){
			project.setDay();
		}
		else if (destinationPage.indexOf("_hc") > 0){
			project.setHc();
		}

		if (doingHash){
			doingHash = false;  // azzero il semaforo dell'Hash
			console.log("  new_pageChange - Finished loaging Hash page");
		}

		if(destinationPage==targetPage){
			console.log("  new_pageChange - Confirmig new page");
			return null;					//se sto andando alla pagina giusta, lascio fare
		}else{
			console.log("  new_pageChange - redirecting to: "+targetPage);
			return targetPage;			 //altrimenti impongo quella nuova
		}
	}

	function currentThemeString(){
		try{
			var currentThemeId = project.activeThemeIndex; //project.getTag("CurrentTheme");
			var result = project.themes[currentThemeId];
			return result;
		} catch(e){
			alert("currentThemeString - Error: " + e);
		}
	}

	function newTargetPage(viewportRatio,pageName){
console.log("> newTargetPage: " + pageName);
		try{
			// Questa gestione potrebbe essere migliorata (esempio qui tagliamo il "_full" prima di chiamare newTargetPage, ma ora andiamo a modificare "_theme" in newTargetPage...
			// Avrebbe senso fare tutto qui ma andiamo per step...
			//var pageThemeIndex = pageName.search(/(day|hc)/g); //Usare l'indice costerebbe di meno, ma js non è così smart e mi scoccia implementarlo a mano...  // search > -1 == found
			var targetPageName = pageName.slice();
			var srcTheme = pageName.match(/(_day|_hc)/g);
			var destTheme = "_" + (currentThemeString() || "day");
			if( srcTheme && (srcTheme != destTheme) ){
				targetPageName = pageName.replace(srcTheme, destTheme);
				console.log("newTargetPage: " + pageName + " → " + targetPageName);
			}
		 /* if(viewportRatio<0.5){		 //Column
				if( pageMgr.hasPage(pageName+"_col")){
					targetPageName =pageName+"_col";
				}else{
					targetPageName = "control1_col";
				}
			}
			else */ if (viewportRatio < 1 ){	//Half
				if( pageMgr.hasPage(targetPageName+"_half")){
					targetPageName =targetPageName+"_half";
				}else{
					targetPageName =project.homeHalf;
				}
			}
			else if(viewportRatio<3 ){	//Full
				if( pageMgr.hasPage(targetPageName+"_full")){
					targetPageName =targetPageName+"_full";
				}else{
					targetPageName =project.homeFull;
				}
			}
		/*   else if(viewportRatio>=3){	 //Row
				if( pageMgr.hasPage(targetPageName+"_row")){
					targetPageName =targetPageName+"_row";
				}else{
					targetPageName ="home_row";
				}
			} */
			else{
				targetPageName = project.homeFull; //Default
			}
			console.log("newTargetPage: " + pageName + " → " + targetPageName);
			return targetPageName;
		} catch(e){
			alert("newTargetPage - Error: " + e);
		}
	}//newTargetPage


	/******** LISTENERS **********/
	try{

		project.on( "shouldChangePage", new_pageChange );
		project.on( "resize", new_resize );

		// nei progetti Ferretti non si usa la rotella
		//document.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
		//document.addEventListener("mousewheel", MouseWheelHandler, false);
		//window.addEventListener("keydown", dealWithKeyboard, false);

	}catch(e){
		console.log("Error: "+e);
	}


	//-------------------------------------------------------------------
	//----------------------GESTIONE NA16 OFFLINE------------------------
	//-------------------------------------------------------------------
	var connStatus;
		var noConnDiv = document.createElement('div');
			noConnDiv.style.display = "block";
			noConnDiv.style.visibility = "visible";
			noConnDiv.style.position = "absolute";
			noConnDiv.style.left = "0";
			noConnDiv.style.top = "0";
			noConnDiv.style.height = "100%";
			noConnDiv.style.width = "100%";
			noConnDiv.style.background="black";
			noConnDiv.style.overflow="hidden";
			noConnDiv.style.textAlign = "center";
			noConnDiv.style.verticalAlign = "middle";
			noConnDiv.scrolling="no";
		var img=document.createElement("img");
			img.style.position="absolute";
			img.src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVQAAAGnCAYAAADolK8CAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAPYQAAD2EBqD+naQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7d13nGRVnf7xz3eGnAUUFFYJkhUDIiAKqCCsaRUUlRXEn4ARBMGIq7LrsmaiCVZXESUohjUBigtIEgMiuuSMhBUQkJlh4vP741aPTdM9U1V9Qt2q5/161cuxpzjne6puP3Oq7r3nhCQxGn4UEa+oXYSZDa8ZtQswMxsWDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCwRB6qZWSIOVDOzRByoZmaJOFDNzBJxoJqZJeJANTNLxIFqZpaIA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0tkmcztXwhc2sPz9wWelKkWM7OscgfquRHx790+WdJOOFDNrKX8kd/MLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiuW89fbOkXXp4/pa5CjEzyy13oG7ceZiZDT1/5DczS8SBamaWiAPVzCwRB6qZWSIOVDOzRByoZmaJOFDNzBJxoJqZJeJANTNLxIFqZpaIA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiDlQzs0Ry73p6HqAenr8tsHqmWszMssodqLtHxMJunyzpEmCHjPWYmWXjj/xmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCwRB6qZWSIOVDOzRByoZmaJOFDNzBJxoJqZJeJANTNLxIFqZpaIA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEpkBKGP7kfn5ZmYDYwYwJ2P7y2d+fi8WZGzbzIwZwMMZ21+xx+evkKWKxryMbZuZMQOYlbH9VXt8fq8B3AsHqplllXuG+sQen79WlioaszO2bWbGDPIGzXrdPlHSSvQ+o+3FvRnbNjPLPkP9hx6eu262KhoPZG7fzEbcDODujO1v1cNzN85WReMvmds3sxE3A7gpY/tP6+G5W2SronFj5vbNbMTlDtStJC3T5XM3z1gHOFDNLLMZwM0Z218ZeFaXz90uYx2zgLsytm9mxgzyz9xesLQndM7wb52xhhsiIucttmZmzADuBB7J2McLu3jOdkC3Xw3044aMbZuZATAjIhYB12Xs48WdGeiSvDRj/+BANbMCxpbvuzRjHysCuy3lOa/M2D/A5ZnbNzNbHKiXZO5nz6n+QtKWwKYZ+xZwccb2zcyAcoG6l6RVpvi7N2Xu+7qIuCdzH2ZmTaBGxA3kvWNqZSaZpXauUX1jxn4BLsrcvpkZ8OgtUC7L3NdBk/zsn4AnZe7XgWpmRYwP1F9m7mtHSdtM+Nn7MvcJ+cdlZgY8OlB/UKC/Q8b+IOmFwHMz93ddRPiWUzMrYnGgdoLn95n7e4OkjTp//rfMfQGcWaAPMzPgsdtIn5W5v2WBD0p6DbBj5r4Avl2gDzMzYMK2zZI2B67O3Od8moVKnpy5n+siYrPMfZiZLfaoGWpEXAP8KXOfy5I/TAHOKNCHmdliEz/yw/B87/id2gWY2WiJiT+Q9ETgFmC54tWk87uImHiJlplZVo+ZoUbEXbR/dndM7QLMbPQ8ZoYKIGl78q5AldNdwAYRMa92IWY2Wib7DpWIuIz2Lnn3BYepmdUwaaB2HF+sinTmAifXLsLMRtOkH/kBJC1Hs9/U+uXKmbavRMQBtYuw7nWOs3WANSc8AliDxx6jAh4AFgH3T3jcExHzy1Ru9lhTBiqApLcCXypUy3TNA7aIiJzbYlsfOlvgbA5sBWwJbEhzLfJTgHVZ8ielXiyiWYbyFuB2mh19/wT8L3B1RMxJ1I/ZpJYWqMsC19L8Agy64yLi0NpFjDpJK9BsHf5cms0XtwU2Il1o9msRzSeuX9OcH/gVcEVEzK1alQ2VJQYqgKR/Bk4tUMt0PAg8NSLurV3IqOksEr4d8OLOY3vacw3zPJqrWc7rPC6PiAV1S7I26yZQg2ZPph3yl9O3IyPi6NpFjApJjwNeRrNA+O7AqnUrSuYh4Bzg+8BPIuKByvVYyyw1UAEkPZfmX/LaH9sm82dg04iYXbuQYSZpdeA1wOuBnWnWZBhm84DzgdOBsyLiobrlWBt0FagAkk4E3pmxln69OSK+VruIYSRpJrAHsC/NbHSFuhVVM4dm1noqcE5ELKxcjw2oXgJ1JeBK4Kn5yunZecBuEaHahQwTSesA+wNvpzkTb393J/AN4ISI+HPtYmywdB2oAJJ2Bn7BYHz0/xvw9Ii4tXYhw6Kz59fhwF6058RSLXNpFjD/bETk3unCWqKnYIyIC4ATMtXSq/c6TNOQ9HxJP6S5pOgNOEy7sTzNFuhXSLpI0itqF2T19TRDhcUf/X8PbJK+nK79D/Bif9SfHkm70uzttX3tWobExcCHI+L82oVYHT0HKoCkHWjOgNaYycwCtvYdUf2TtB1wNPCi2rUMqXNoLuX7be1CrKy+vguNiEuBdyWupVsHOkz7I2kDSd+huQTOYZrP7sCvJZ0mqU1rYdg09X1yKSJOBj6fsJZuHBsRpxXus/UkrSjp/cBVNCec+vpkYj0Jmmt2r5X0sc4tuTbkpvWL1bnX/1xglyTVLNkvab439WpCPZD0cuBEfPlTbTcC74iIc2sXYvlMe6YiaU2axSY2nn45U7ob2CYi7szYx1Dp3B76CeCg2rXYo3wbeHtE3Fe7EEtv2teTRsT9QM41SOcDr3GYdk/SG4DrcJgOotcCf5T06tqFWHqpLtC/O1E7k7klIi7O2P7QkLSapFOAbwFr167HprQu8F1Jp0hapXYxls4g3PFkCXQuhfodzX331g77An/oXIZoQ8CB2nKSQtJ7gYvI+z225bEhcL6kQVx4yHrkQG0xSasCZwCfApapXI71bzngREnf7NyJaC3lQG0pSZvTXF3x2tq1WDL7ABdL2qB2IdYfB2oLSdqR5rrczWvXYsk9E/iNpOfXLsR650BtGUn70yyh6LP4w2st4Oedy9+sRfy9W4tI+ijwsdp1JPJX4GqarZ5v6TzuAe4b91gIzB3b3qbz/eLywEya0Fm787/rABvQ3A22Ic1W1WuUGkgmywOnSlonIo6tXYx1x4HaAp2NEo8DDq5dS58eAC6hWZTld8BVEXF7r410gnVs77B7abY4n5SkJwNPB55Ns8Hk84DVe+2zshnAMZIeFxEfrV2MLV2SRTI6J0iuTtHWJK6PiE0ztT3wOts0f5V2XV86F7gQOJtmrYf/jYhFNQuSNAN4GvASmn2ynk8zC2yL44FDvQbwYHOgDrDO4jOnA3vWrqULs4EfAWcCZ0fErMr1LJGklYGXAnvTbIm9Yt2KunIy8FaH6pCTtLnyua72+GqQNFPSGRlf1xQWSfofSf+sFl8/KWkVSftKuqAzpkF2opqvgGxYyYGalJowPTXjazpdD0j6jKSh++QgaTNJn5X0YNVXeMk+V/t1sozkQE1K0kkZX8/puEXSYWru0BpqahaaOVzSbVVf8akdVfs1skzkQE1G0r9mfC37dZukd0tq00mcJCQtK2k/STdVfQcmV2sbIstJDtQkJL0t4+vYj3slHawRDNKJJK0g6VBJ91V9Rx5tgaRX1n5tLDE5UKdN0ss6vyCDYJ6kY9Ws+m/jSFpT0gmS5ld9h/5utqTn1H5drOFbTweApC1oFoWeWbsWmgvwnxkRh0bEX2sXM2gi4v6IOBh4FnBZ7XpoLvf6nqR1axdiDtTq1MwCfwCsVrmU2cAHgJ0i4n8r1zLwIuKPNHdfvRX4W+Vy1qfZAWDkv5qpzYFakZq7d04HNqlcysXAVhHxyYhYWLmW1ogIRcRJNCtEXVq5nB2AEyrXMPIcqHV9mOZWyFoWAJ8EXhgRt1Sso9Ui4iZgJ5oZfs1tzg+U1KZblG0y8kmpnknaRXVPQv2fpBfVfh2GjaQXSLqr4vv6NzW3glsFnqFWIGkd4DTqnYQaO/H0i0r9D62I+CWwLfVOWK0CnCZphUr9jzQHah0n02wlXMMpNB/x76zU/9CLiDuAXYBvVirhmcC/Vup7pDlQC5P0FuAVNboGjgL2j4h5FfofKRExl2bJxaNoXvvSDpe0U4V+bbrk71C7IukpqrPoxgJJ/6/2+EeVpAMlLazwvl+nFq8C1kaeoZZ1EuWvN50PvDEivlq4X+uIiJNpZqsLCne9CfDxwn2ONAdqIWo2XCt9idQ84LURcXrhfm2CiPgWzZbfpS+rOljSMwr3ObIcqAVIWg34TOFuFwJviogfFO7XphAR3wdeT9mZ6jLAyWpuIrHM/CKX8QngSQX7WwS82TPTwRMR3wUOpOyJqm2BtxTsb2Q5UDOT9HTgoMLdHhER3yjcp3UpIr4GvL9wtx/vfFKyjByo+X2GshfwnxQRxxTsz/oQEZ8GPl+wyydQPsRHjgM1I0l7UPZE1I+BdxTsz6bnUJqttks5TNL6BfsbOQ7UTDonAT5VsMvrgX/2alHtERELgH2Amwp1uSK+gyorB2o+ewNPL9TXLGDPiHiwUH+WSGcR71fTrEdbwn6Sai8XObQcqBl0Zqf/UrDLd3QWPLYWiog/AIcU6m4mzTKDloEDNY/XA1sW6uusiDilUF+PIdhD8N5a/acieK9gj1r9R8RXgDMKdbevpA0L9WW9ku/lX0xSSPpjxtdjvFslrVFtrPASwRyB1CyW3UqCIzpjmKs6C9c0dTQbAN5e6Nj5Qq1x2lLIgbqYpH/M+FqMt0jSbtXG+egwHXscWauefgkOnzCGuYKXV6un3PEzS9KatcZpSyAH6mKSfp7xtRjvv6qNcfIwbV2oThKmgxKq3yx0DPm71EEkByoAkrbO+DqMd7cqzS6WEqatCdUlhGn1UJX0eEn3FjiObpe0bI0x2hLIgQqApJMyvg7jvbnK+LoL04EP1S7CdBBC9W2FjqW9aozPlkAOVCStojKLR/9OFVYO6jFMBzZUewjTqqEqaaakKwscTz8uPTZbCjlQkXRAxtdgvBcUH1sTprN7DKKBC9U+wnTs8UilUH1RgeNpgXw76mCRAxVJl2d8Dcb8qPi4phemAxOq0wjT2qF6boHjqvr7Y+NoxANV0mYZxz9mkaRti44rTZhWD9UEYVotVCU9p/Pe53RNyTHZUsiB+pGM4x/z3aJjgt0Shmm1UE0YpuND9WVFxyD9sMDxtXXJMdkSyIH6p4zjH7NdsfHkCdPioZohTKuEqqTnFji+/q3UeGwpNMKBqjLXnl5QbDx5w3Ts8aEC43hP5jGUDtWLMx9j15YayzDz4ijT96oCfXyuQB8IdgN+QLNuZk7/njNUBe8BPpur/Y7lgbMKhmru8WwqafPMfVg3NNoz1F9lHLsk3SYp+xYqhWam2WeqBWamVWaqkmaoWQwnp8Nyj2PYeYY6DZIeDzwnczf/mXsV/oIz04mSzlQLzUwnKjJTjYhFwNdz9kHF5QttHI3oDFXSmzKOW5IWSnpy1jHAeoJZhWd1Ex/TXk9VzXqmNccwS7BeivdkyjFK/6DmQvxcHpG0cs4xDDvPUKcn9/J5P4+I23J2EPBnms3ilLOfpfjUdGaqnZlpyf27JimB93dey2wi4nYg5wnK5YEdM7Y/9FIF6rxE7Uxm1YxtT1fu20CLrOAecDLwVuqGal8f/yt9zJ9QAocEnFiovzMzt+9ArU3NcmO5LFCBkzK9krRBxjFL0jwVXqJPcKBgUeWPzl2HqsqfgJr4WCQ4OOd78pgxS2tLmp/xuPt5yfHYJCStkPENlqSNao9xIkn7ZR7zT6uMqyWhOophunjs0s8yHncPS1qmxriGQZKP/BHxCPBIiram8NKMbfdrh8zt/yRz+5Nqw8d/DcbH/HcHnFCp/5zHxsqU2/7cpqK8G9P9ovb4JlL+60+r7p0+IDPVD05S12GjOjNd/BpIW2Q+9qosYG7jSDor85tcfB3QqUhaRtKcjGO9ofYYAQQHDVKoOkz/TtLNGY+/Y2uPr61SfleSewmwT0t6fkQsyNxPN7YAVsjY/vkZ2+5awElq/vil5v9WcXSnhkcodAvuFETdj/kTXQBskKntZ2Rqd+ilvA71Nwnbmsx21P3ebLxnZm7/ksztdy3gJOAQqPqd6tHUD9NDBihMAS7O2La/Q+1TykA9H8h6iyRwiKTHfK9WwaaZ2x+YQAXoXGP5NuqGai1jM9NS15l2K2egrqVKu+q2XbJAjYi/Alekam8JjpZ0iqRVCvQ1lZwnjB4ABm4ptc5MddRCVcChAzYzHXM18GDG9jfO2PbQSn3r6TmJ25vKvsCNkt4laaVCfY6XM1CvioiBDK0RC9WxMD2+diGT6Rwjf8zYhQO1D6kv4D2VciuyP4Fm5vApSecBvwTuBP4PWJS575yB+oeMbU/bgJyoym2gw3Scq8h3q+iLJd2bqe0xD4973BsRD2fuL7vkvxCSLqM5gWT9eVtEfLl2EUsjOIjhDNW2hCmS3sngfbc7HX+h+SrjWuBy4BcRcVPdknqT4xaz/8KBOh0DcQ3q0gzpTLU1YdrRimOlB4/vPHYCDoTmelvgbOAbEXFpxdq6kmOGuiJwE7Bu6rZHxCYR0ZpfFDW3qX6R9odq28IUSZuR//rvQXItzYTtSxGR84Rc35KvhxoRc6h7zWCbLQKyrn+aWsCXgbfT7hNVrQvTjlvJf75gkGwGfAK4XdJxktapXdBEWWYVnUuabqKZvlv37oqIJ9Uuoh8tnqm2NUwBkHQ3MHDBUsiDwEeAz+feJqhbWVbs75yt+0COtodc7rOq2bR0ptrqMO1o7TGTwOrAccCvJW1buxjIuwXKf5F3u4Zh1OpfjpaFqoDDWh6mAPfVLmAAPAu4RNLHJFXd1ilb550Ljw8G5ubqYwjdX7uA6WpJqI6F6XG1C0nAgdpYBvgo8IOat81mTfOIuAp4X84+hkzrL2yGgQ/VYQpTgFm1CxgwLwcuUubdgqeSfXocEccDZ+XuZ0jk3OywqE6oHsbgheoHhyhMwZ8AJ7MFcJmkrUt3XOr7hgOAPxXqq82GJlABOsH1HgYnVD8Q8MnaRSTmQJ3cE4HzJG1RstMigRoRDwB70LJrLCsYhMWzkwo4FvhB7TqA7w9hmALMr13AAFsbOFvS+qU6LHZGLCLuAHan5WeyMxu63SYFhwKvql0H8CoN56V8y9YuYMA9mSZUVy/RWdFLDCLiGpr7dD1TndxytQtIqROmx9SuY5z/ELy/dhGJLV+7gBbYCjhFUvabTopfsxURV9NswXxV6b5bYGgCtROmg3gL8ieGLFQdqN15JfCu3J1UuQg2Iu4EdgZ+WKP/AVZzF4JkxoXpoN6GOkyhunLtAlrk05Ky7pdV7a6CzpYp/0TzyzdUZ7enYa3aBUxXC8J0zLCE6tq1C2iR5YEv5vzoX/U2rYhQRBxHs+r4b2vWMiBaHaiCd9OOMB0zDKHqzfR6syPw5lyND8yBL2kmzd01H6dZ9GAUebWp8lp959SIrzbVr78AG+XYcqXqDHW8iFgYESfSXObwAYbgvvY+rCOpdScZWhym0NR8TGd23SqSVqDZW81683iaYza5gf0FkLQazQ6b+9PcSjYqvGJ/Ha2bqY7giv0p3U0zS52TstGBmaFOFBEPRcSnImJLmj2qTgRurFxWCRvULqBbQxSm8PeZ6iG1C+nBhrULaLF1abajT2pgA3W8iLg8Ig6OiKfSHEQH0OwRfz5wT83aMnhq7QK6MWRhOiaAY1sUqhvXLqDl9k/dYOtudYyIW4CvdB7A4i1X1gRWpbmWc9XMZbyOJtRzyHqdXApDGqZjxkKVFiw+nfNYeQjYK2P70LzW69DMFncCdgVWzNzneDtI2jQirivYp00k6fXK58La41sSwVsFiwQa8seiQZ+pSro443H4mwrjWUnSuyTdk3FcEx2Vcgyt+Mg/gG7K2PbTVeCe434M+cx0ooH++N85Rp6WsYvis7aImN250mdj4NRC3e6RsjEHan9yBuoaNNvlDhTBQYxOmI4Z5FDdElgtY/vVPgZHxMMRsS9wZIHutpG0RqrGHKh9iIh7gQcydrFjxrZ7pmZRiS8xWmE6ZixUsy+s0aPcx0j17xUj4mjghMzdzAR2SdWYA7V/f8jY9vMytt2Tzsz0eOqG6YdoVv6vJYDjB2ymmjtQf5+5/W69B7g8cx/bZG7flkbS8Rm/KB+I620FBw3ACagPjqvnMJ+oaki6JePx97CaW8EHgqSdM45Vkr5de4wjT9JbMr/Jm1Yd34CF6bi6BiFUD67xnix+DaQtMx97l9Qc32QknZ9xvMnWZvZH/v5dmbn9pGcfe6HmY37t70w/FPAfE38YzQ4AtT/+H1c5VHMfG7/L3H4/vpuxbd8gUZuk5STNyviv5tlVxjWgM9NJ6hzZmaqkn2c87iRpnxrjWhJJT8085qHZLaO1JP1Pxjd4nqSi66O2JUzH1TtyoSrp8ZLmZzzuJOkfSo6pG5JmSlqQccxJftf8kX96Ls7Y9rLAqzO2/yiCAxnQj/lTGdGP/68h7y3jN0bE7Rnb70tELKRZxzSXJNf0OlCnJ2egAuyduX1gcZh+mRaF6ZgRDNXXZm7/l5nbn46/ZWy7desQDx1JK0t6JOPHkIWSnpJ1DLCeYFblj87vTTCO91YewyzBeinekynHKG3QOSZyGrjvT8dIui7juDdPUaNnqNMQEbPIO0udQYYlxsYL+DPwKiDpQrs9ODLg09NtpNPG4Qnq6cdcYO/Oa5nTW8j7O7sQOCdj+2ZLJumIjP9qStLtKnCRtWA3wezCs7oPZRjHewqP4RHBy1KP4zHjak7K3Jb5WLso9zimQ56hjoTclzetD7wycx8E/IxmW+9SM9UjA45O3Wg0u66WmqnOBfYK+HGBvvYEcp99/0nm9s2WTtL1Gf/llArOHArNVJPPTCcZR+6ZapGZ6eLxSJdmPsYkactS4+mHWjBDtQQk/UfGN3rM9sXGkzdUs4fpuHHkCtXSYfqCAsfXoCyGMiU5UEeDpGdnfKPHfL/omPKEarEwHTeO1KFaNEwBJP24wPHV9Q0VtciBOjqU/2O/JD236JjShmrxMB03jlShWiNMt5W0qMCxNfCbQ8qBOjokfSzjmz2m+EmDRKFaLUzHjWO6oVo8TAGU/759acD3MRsjB+rokPQU5b/oWpJ2Lj626YVq9TAdM41QrRWmuxY4niRpv9Jj64ccqKNF0rkZ3/AxV6jC4r99hmqJPYF6Iji8JWE6U9IfChxPD0haqfT4+iEH6miR9LqMb/h4b6kyPnhJD6E6cGE6podQfUTw8io1Su8odCydWGN8/ZADdbSoWSP1zoxv+pj/U+Gl/RaPsbtQHdgwHdNFqNYM0ydIuq/AcbRI0hY1xtgPOVBHj6QPZ3zTx/tatTEuOVQHPkzHLCFUq4UpgKTTCh1DP6o1xn7IgTp6JK0taXbGN368l1QbZxOqc9oapmMmCdW5lcP0pYWOHUl6Ua1x9kMO1NEk6aSMb/x4t0l6XLVxPjpUWxemY8aFau0wXUvSHYWOnUHcN2qJ5EAdTZI2Vv5tKsZ8p+pYYQ8lWM+0NjXrqVbbGBFA0hmFjhlJ2qvmWPshB+rokvS1jG/+RPvXHq9Nj6QDCx4vV0qquTtDX9SCQPXyffl8HFhQqK8vSnp2ob4sMUnPAI4r2OVREaGC/Y0MB2omEXED8M1C3a0AnKmK36dafyStCXwfWLFQl78Hvleor5HjQM3rI5RbsHlj4FuScu6IaQl13qvTgQ0KdnuEZ6f5OFAziojbKPtRbg/giwX7s+k5HtitYH//HRHnFexv5DhQ8/sEefcTn+gASUcU7M/6IOkDwNsLdjkfeF/B/kaSAzWziHgQ+JfC3X5KPvM/sNSsxZB8P62lOCEiri3cp1l6kmZIuiTjJR+TWSDp9bXHbo8maU+Vu0Z5zG2SVqk99umSL5sygIhYBLyTZt/zUmYCp0h6dcE+bQkk7UlzEqr0icN3R8TDhfscSQ7UQiLiCuCEwt0uS3M51T6F+7UJJL0ROIPmPSnphxHhy6QKcaCW9WHghsJ9LgN8Q9JBhfu1DklvB75O+Znp/cDbCvc50hyoBUXELGA/yn70h+Z9/rKk4yT5PS9EUkj6GPAF6vyuvTMi7qzQ78jyL1dhEXEpcGyl7g+hma0uX6n/kSFpBeBbwEcrlfDtiDi9Ut8jy4Fax4eBKyv1vQ9wgaT1K/U/9CQ9GbgQqHWVxe2UvcbVOhyoFUTEI8BrgYcqlbAdcIWkXSv1P7TU7Ep7ObBtpRIWAPtExH2V+h9pDtRKIuJ6oOaJorWBsyV9QlLpM89DR9Iyne9LzwPWqVjKkRFxUcX+zeqRdHLGi5W7dYmkjWq/Fm2lZkHxyyq/h5L0Q7VwndNuyRf2WxfeQfN9W007AFdJer+kmZVraQ01d8AdBFxB8zVKTdcA+3olqbocqJVFxHzgdcAdlUtZiWYhl4skPb1yLQNPzaLQlwFfBlatXM79wCsi4oHKdYw8B+oAiIi7gVcBs2vXAmwP/E7SiZLWql3MoFGzq+0Xgd9S78TTePOB13UWNLfKHKgDIiJ+C7yB8hf9T2YZmrUHrpd0mJprKkeapBUlHQ5cT3P30SB8NSLgwIj4ee1CrOFAHSAR8d8M1q2CjwM+B9za+X515IJV0nKd70lvAD4DrFG5pPE+FBFfr12E2UCTdFTGs5nTcbukIyStXvs1yk3S6pLeJ+mOqq/41EovtFOdWnCW3waUpGMyHjzT9aCkz2kID0JJW6h57R+q+gov2Vc1gmsyyIFq/VKzsMaJGQ+gVC6UtJ+k2me6+yZpNUn7S7qo6ivZnVM0gmEK7QjUob0IeBiouUj7y8CBtWvpwhzgp8CZwE8i4m+V61kiSasBLwP2ptncsA3fD59Gc63pIJy4LE7SdcAmmZrfIiKumW4jDtQB1wnVY4B3166lB/OBi4CzgXOBq2qHzU5HNwAACKBJREFUgJobFp5Bs8voHsCOlF/seTq+ChxU+3WsyYFqyUg6CvhI7Tr69BBwKXAJzV1FV0XELbk66/wjtAHwNODZwPNo7gZr69cSxwCHj/pdUA5US0rSocBnGY7L3R4CrgZuBm7pPO4G7hv3mA/MH9sPSc1Gc8t2HmuNe6xLE6Abdh6bA6uVGkhGAj4aEf9Wu5BB4EC15NRsuncqza2iNrzm0Vy0f0rtQgZFGwJ1GGY6I6Wz4doLgXtq12LZ3A+8xGHaPg7UFoqIy2nuuf997VosuT8C20XEBbULsd45UFuqc1JnB+ArlUuxdE4DtvdCJ+3lQG2xiHgkIg6gWchkXu16rG/zgUMjYp/OzrjWUg7UIRARXwC2Aa6qXYv17GZgl4g4rnYhNn0O1CEREX+kWTX++Nq1WNe+AWwdEZfULsTScKAOkYiYExHvBl4N3FW7HpvS3cBeEbHf2DW2tmSSdqS51jiXdVM04utQh5SkNYBP0qwD4Pd5cHwbeEdE3Fu7kLaQtC7wG2C9jN3cCmwbEX+ZTiOeoQ6piHggIt4K7E6zOLLVdSOwR0Ts7TDtnpotzr9D3jAFeApwpqa5pboDdchFxM+ALYFDgYFeAWpIzQaOAp4WEefULqaFjqFZyKaEXTr99c0fBUeIpPVpvgZ4A37vcxPNdaXvj4jaO9q2kqT9gBpbvBwYEf/Zz3/oX6oRJOlZwL8D/1i7liH1U+DIiLiidiFtJWkTmjsBa6xZMQ/YOSIu6/U/dKCOMEk7AR8HXlC7liFxIfAvEXFh7ULarLMjwfnUPS6vB54ZET1t7e7vUEdYRFwYETvRHLg/ovmYar27GNg1InZ2mCbxLur/I78JcHSv/5FnqLaYpK2B9wCvox1bgtT0CHAG8NmI8B1qiUjaCPgDsHLtWoBFwAt7+UfSgWqP0bmG9U00265sWLmcQXMncDLw+eles2iP1tlp4RyabWoGxc3AM7rdI82BalPq7MO0G/BGmruvRnVR69nA92gW9v7ZKO/rlJOkA4GTatcxic9HxLu6eaID1bqiZpvoPYHX0yxwvXzdirKbC/wCOB343qDv4tp2ne1trifRLaCJLQKe081VGw5U61lnC+Z/pJm17g6sUbeiZB6g2an1e8BPHaLlSPoo8LHadSzBzyLiJUt7kgPVpqXztcA2wK7Ai2juamnLCa05NGfoz6OZjf7WH+fLk7QOze3Rq9SuZSl2j4hzl/QEB6ol1bkX+pk0SwluBzwX2BiYWbMuYCHNL+3lncdlwJURMb9qVYakLwBvr11HF34PbBMRi6Z6ggPVspO0As3WzlsAW9Esw/aUzuNJpAvbhTRn4W/tPG4G/gRcA1wdEXMT9WOJSNqUZh+taS1KUtB+EfGNqf7SgWpVSVoGWAdYc8JjJs1HwIm/aPOBh2nC8/4Jj3siYkGZyi0FSV8H9qtdRw9uBTaZ6pONA9XMqpD0BOA22nfFyD4Rcdpkf+FbT82slnfSvjAFOGSqv/AM1cyKk7Q8zcfndWrX0qftI+JXE3/oGaqZ1bAP7Q1TgIMn+6FnqGZWnKQraC6va6t5wAYR8ajNMD1DNbOiOguctzlMAZYDDpj4QweqmZW2d+0CEnnMOByoZlbaawv1c2Pm9p/WuTFhMQeqmRUjaRuaW5Fzuxx4KZD7Ro+9xv8fB6qZlVRqdnpERFxHs6tCTo8KVJ/lN7NiJN0IbJS5m59GxEs7/W0FXEW+rBOwcUTcDJ6hmlkhkjYjf5gCfHLsDxHxJ5rlGXMJ4J/G/o8D1cxKKbGT6a8i4oIJPzs+c5+Lx+VANbNSnl+gjy9O8rMfA3dk7HPHsT84UM2slNyB+jBw1sQfdhaE/lbGftfpbH/tQDWz/CQ9kfyXS50ZEQ9P8XenZO77eeBANbMySnx/+r2p/qJzcuqajH07UM2smOdkbn8OzUaLS/LDjP3vAA5UMytjk8ztXxARs5fynJyBupmkcKCaWQlPzdz+RV0851fA0kK3XysCT3SgmllWkoL8J6QuWdoTImIe8OuMNWzkQDWz3NajmcHlIuA3XT73lxnrcKCaWXa5vz+9IyL+1uVzr8xYhwPVzLLbMHP71/bw3KuyVQEbO1DNLLfHZ26/l4WkbwDmZqpjXQeqmeW2Vub271r6UxoRsRC4M1MdKzlQzSy3x2Vu/y89Pv/PWaqAVRyoZpZbzjP8APf2+PyuZ7Q9cqCaWXbLZ25/To/Pn2oBlela2YFqZrktm7n9Xk8y9RrA3XKgmll2MzO3/+wen58rUH1Sysxab+cen78oSxUww4FqZpaIA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCwRB6qZWSIOVDOzRByoZmaJOFDNzBJxoJqZJeJANTNLxIFqZpaIA9XMLJFlahdgZjZN20r6WQ/P3yRXIQ5UM2u7tYBdaxcB/shvZpaMA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCwRB6qZWSIOVDOzRByoZmaJeNdTM2u73wJ/7fK5Abw4VyEOVDNruyMi4vxunihpJrAgVyH+yG9mlogD1cwsEQeqmVkiDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCwR33pqZm33BknbdfncrJNIB6qZtd1BtQsY44/8ZmaJOFDNzBJxoJqZJeJANTNLxIFqZpaIA9XMLBEHqplZIg5UM7NEHKhmZok4UM3MEnGgmpkl4kA1M0vEgWpmlogD1cwsEQeqmVkiDlQzs0QcqGZmiThQzcwScaCamSXiQDUzS8SBamaWiAPVzCyR/w8wYUjpZXFJOwAAAABJRU5ErkJggg=="
			img.style.height = "50%";
			//img.style.width = "50%";
			img.style.top = "0";
			img.style.top = "0";
			img.style.left = "0";
			img.style.right = "0";
			img.style.bottom = "0";
			img.style.margin = "auto";
			img.style.overflow = "auto";

			noConnDiv.appendChild(img);

		var par = document.createElement("P");
		var txt1 = document.createTextNode("CONNECTION WITH OP-BOX LOST");
			par.appendChild(txt1);
			par.style.fontSize = "xx-large";
			par.style.color = "white";
			noConnDiv.appendChild(par);

	function checkConnectivity(){
			connStatus=project.getWidget( "_SysPropMgr" ).getProperty( "Connection status" );
			console.log("Status: "+connStatus);

			//non ho molta più diagnostica quindi conto
			//0 = client can not reach the server client. The connection with server is lost.
			//    ND4.0.1.354 applica già un suo timeout, per cui Connection status non va a 0 prima di un tot.
			//1 = client can reach the server. The connection with server is active.

			if(connStatus==0){
				connStatusCount++;
				if(connStatusCount>0){
					document.body.appendChild(noConnDiv);
				}
			}else{
				connStatusCount=0;
				if(document.body.contains(noConnDiv)){
					document.body.removeChild(noConnDiv);
				}
			}
	  }
	window.setInterval(checkConnectivity, 2000);


	//-------------------------------------------------------------------
	//-------------------------------------------------------------------

	console.log("Init Done");

} // ClientType






/********* BACKGROUND COLOR *********/

project.setDay = function(){
   setBackgroundColor("rgb(10,27,38)")
}

project.setHc = function(){
   setBackgroundColor("rgb(255,255,255)")
}

function setBackgroundColor(colorString){
	var cont = document.getElementById("jm-container");
	cont.style["background-color"] = colorString;
}

