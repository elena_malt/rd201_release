function Dump_onDataUpdate(me, eventInfo)
{
    project.setTag("DumpAlarmsDisable",1);
    page.setTimeout(function(){
        project.setTag("DumpAlarmsDisable",0);
    },3000);
    return false; 
}
function MainAlarm_onDataUpdate(me, eventInfo)
{
    if (eventInfo.newValue > 0){
        project.setTag("BuzzerControl", 2);
    } else {
        project.setTag("BuzzerControl", 0);
    }

    return false; 
}
